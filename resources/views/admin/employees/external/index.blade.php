<?php //dd($employees)?>
@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
		@if($type == 2)
			{{ trans('cruds.employee.title_singular') }} 
		@else
			{{ trans('cruds.employee.internal_certificate') }} 
		@endif	
        {{ trans('global.list') }}
		
		
		

	
@can('employee_create')                        
	@if($type == 2)
		<a class="btn btn-warning float-right" href="{{ route('admin.employees.create') }}">
			{{ trans('global.add') }} <?php /*{{ trans('cruds.employee.title_singular') }}*/?>
		</a>
	@elseif($type == 3)
		<a class="btn btn-success float-right mr-2" href="{{ route('admin.members.create') }}">
			{{ trans('global.add') }} <?php /* {{ trans('cruds.employee.internal_certificate') }}*/?>
		</a> 
		<a class="btn btn-warning float-right mr-2" href="{{ route('admin.employees.create') }}">
			{{ trans('global.add') }} <?php /*{{ trans('cruds.employee.title_singular') }}*/?>
		</a>
	@elseif($type == 4)
		<a class="btn btn-success float-right" href="{{ route('admin.members.create') }}">
			{{ trans('global.add') }} <?php /*{{ trans('global.internal_employee') }}*/?>
		</a>
		<button class="btn btn-warning float-right" data-toggle="modal" data-target="#csvImportModal">
			{{ trans('global.app_csvImport') }}
		</button>	
		@include('csvImport.modal', ['model' => 'Member', 'route' => 'admin.employees.parseCsvImport'])					
	@else
		<a class="btn btn-success float-right" href="{{ route('admin.internalemployees.empSearch') }}">
			{{ trans('global.add') }} <?php /*{{ trans('cruds.employee.internal_certificate') }}*/ ?>
		</a>
		<?php /*<button class="btn btn-warning" data-toggle="modal" data-target="#csvImportModal">
			{{ trans('global.app_csvImport') }}
		</button>
		@include('csvImport.modal', ['model' => 'Employee', 'route' => 'admin.employees.parseCsvImport'])*/ ?>
							
	@endif				            									
@endcan	
	
    </div>	
	
<?php if(session('success')){ ?>
    <div class="col-12">
<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true" style="font-size:20px">×</span>
  </button>  {{session('success')}}
 </div>
</div>
<?php } ?>
    <div class="card-body">
        <div class="table-responsive">
			<?php //echo Auth::user()->name;?>
            <table class=" table table-bordered table-striped table-hover datatable datatable-Employee">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.id') }}
                        </th>
						@if(in_array($type,array(1,3)))
                        <th>							
                            {{ trans('cruds.employee.fields.emp_id') }}
                        </th>
						@endif
                        <th>							
                            {{ trans('cruds.employee.fields.emp_category') }}
                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.first_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.last_name') }}
                        </th>
						<th>
                            {{ trans('cruds.employee.fields.emailid') }}
                        </th>
						<th>{{ trans('cruds.employee.fields.department') }}</th>
                        <th>
                            {{ trans('cruds.employee.fields.institution_name') }}
                        </th>
						
						@if($type != 4)						
						<th>
                            {{ trans('cruds.employee.fields.default_certificate') }}
                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.certificate_approval_status') }}
                        </th>
						<th>{{ trans('global.created_by') }}</th>
                        <th>{{ trans('global.reviewed_by') }}</th>
                        <th>{{ trans('global.approved_by') }}</th>	
						@endif
						<th>&nbsp;</th>
						
						<?php /*
                        <th>
                            {{ trans('cruds.employee.fields.certificate_approve_by') }}
                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.certificate_qrcode') }}
                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.certificate_approve_date') }}
                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.employee_type') }}
                        </th>
                        <th>
                            {{ trans('cruds.employee.fields.created_by') }}
                        </th>
						*/ ?>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($employees as $key => $employee)
						
                        <tr data-entry-id="{{ $employee->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $employee->id ?? '' }}
                            </td>
							
							@if(in_array($type,array(1,3)))
                            <td>
                                {{ $employee->emp_id ?? '' }}
                            </td>
							@endif
                            <td>
								{{ App\Employee::EMP_CATEGORY_SELECT[$employee->emp_category] ?? '' }} 
							</td>
							<td>
								{{ $employee->first_name ?? '' }} 
							</td>
							<td>
								{{ $employee->last_name ?? '' }}
                            </td>
                            <td>
								{{ $employee->emailid ?? '' }}
                            </td>
                            <td>
                                {{ $employee->department ?? '' }}
                            </td>
							<td>
                                {{ $employee->institution_name ?? '' }}
                            </td>
							@if($type != 4)	
							<td>
								@if(isset($certificate[$employee->default_certificate]['certificate_title']))
									<a href="/admin/certificates/{{$employee->default_certificate}}/" target="_blank">
										{{ $certificate[$employee->default_certificate]['certificate_title'] }}
									</a>
								@endif								
                            </td>

                            
                            <td>
								<?php //echo '<pre>';print_r(Auth::user()->roles[0]->title);echo '<pre>';?>
								
								{{ App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT[$employee->certificate_approval_status] ?? '' }}
								
								
								@if(Auth::user()->roles[0]->title == 'HOD')
									@if(App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT[$employee->certificate_approval_status] == 'Reviewed')
									<a class="btn btn-xs btn-info" href="{{ route('admin.employees.certificateapprove', $employee->id) }}">
										Approve
									</a>
									@endif	
								@elseif(Auth::user()->roles[0]->title == 'Manager')
									@if(App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT[$employee->certificate_approval_status] == 'Pending')
									<a class="btn btn-xs btn-info" href="{{ route('admin.employees.certificatereview', $employee->id) }}">
										Review
									</a>	
									@endif
								@endif
								
								
								@if(App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT[$employee->certificate_approval_status] == 'Approved')
									<a class="btn btn-xs btn-info d-none" href="{{ route('admin.employees.certificatepdf', $employee->id) }}" target="_blank">
										Certificate PDF
									</a>
								@endif																								
                            </td>
                            <td>
								@if(isset($usersname[$employee->created_by]))
									{{ $usersname[$employee->created_by] }}
								@endif
                            </td>                            
                            <td>
								@if(isset($usersname[$employee->reviewed_by]))
									{{ $usersname[$employee->reviewed_by] }}
								@endif
                            </td>                            
                            <td>
								@if(isset($usersname[$employee->certificate_approve_by]))
									{{ $usersname[$employee->certificate_approve_by] }}
								@endif
                            </td>                            							
							@endif
                            <td>
								@if($type != 4)	
									@if(isset($certificate[$employee->default_certificate]['certificate_title']))
										<a class="btn btn-xs @if($employee->certificate_approval_status == 2){{'btn-success'}}@else{{ 'btn-warning' }}@endif" href="{{ route('admin.employees.certificatepdf', $employee->id) }}" target="_blank">
											Certificate
										</a>
									@endif	
								@endif
                                @can('employee_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.employees.show', $employee->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

								@if($employee->certificate_approval_status == 1)
									@can('employee_edit')
										<a class="btn btn-xs btn-info" href="{{ route('admin.employees.edit', $employee->id) }}">
											{{ trans('global.edit') }}
										</a>
									@endcan

									@can('employee_delete')
										<form action="{{ route('admin.employees.destroy', $employee->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
											<input type="hidden" name="_method" value="DELETE">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
										</form>
									@endcan
								@endif
                            </td>
														
                        </tr>
                    @endforeach
                </tbody>
            </table>
        
		
		</div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('employee_delete')
  let deleteButtonTrans = '{{ $mass_actiontxt}}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.employees.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  @if(in_array(Auth::user()->roles[0]->title,array('HOD','Manager')))
  dtButtons.push(deleteButton);
  @endif
  
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Employee:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection