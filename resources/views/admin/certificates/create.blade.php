@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.certificate.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.certificates.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required">{{ trans('cruds.certificate.fields.template') }}</label>
				<div class="col-md-12">
                @foreach(App\Certificate::TEMPLATE_RADIO as $key => $label)
                    <div class="form-check float-left {{ $errors->has('template') ? 'is-invalid' : '' }}" style="margin:5px;padding-left:30px; border:1px solid #CCC">
                        <input class="form-check-input" type="radio" id="template_{{ $key }}" name="template" value="{{ $key }}" {{ old('template', '1') === (string) $key ? 'checked' : '' }} required>
                        
						<label class="form-check-label d-none1" for="template_{{ $key }}"><a href="/images/template{{$key}}.jpg" target="_blank">{{ $label }}</a></label><br>
						<img src="/images/template{{$key}}.jpg" width="100" style="margin:10px; " />
                    </div>
                @endforeach
				</div>
				<div class="clearfix "></div>
                @if($errors->has('template'))
                    <div class="invalid-feedback">
                        {{ $errors->first('template') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.template_helper') }}</span>
            </div>			
            <div class="form-group">
                <label class="required" for="certificate_title">{{ trans('cruds.certificate.fields.certificate_title') }}</label>
                <input type="hidden" name="created_by" value="{{ Auth::user()->id }}" >
                <input class="form-control {{ $errors->has('certificate_title') ? 'is-invalid' : '' }}" type="text" name="certificate_title" id="certificate_title" value="{{ old('certificate_title', '') }}" required>
                @if($errors->has('certificate_title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_title') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.certificate_title_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="certificate_subtitle">{{ trans('cruds.certificate.fields.certificate_subtitle') }}</label>
                <input class="form-control {{ $errors->has('certificate_subtitle') ? 'is-invalid' : '' }}" type="text" name="certificate_subtitle" id="certificate_subtitle" value="{{ old('certificate_subtitle', '') }}">
                @if($errors->has('certificate_subtitle'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_subtitle') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.certificate_subtitle_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="certificate_details">{{ trans('cruds.certificate.fields.certificate_details') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('certificate_details') ? 'is-invalid' : '' }}" name="certificate_details" id="certificate_details">{!! old('certificate_details') !!}</textarea>
                @if($errors->has('certificate_details'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_details') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.certificate_details_helper') }}</span>
            </div>
            <div class="form-group d-none3">
                <label class="required" for="department">{{ trans('cruds.user.fields.department') }}</label>
                <input @if(!in_array(Auth::user()->roles[0]->title,array('Admin'))){{ 'readonly' }}@endif class="form-control {{ $errors->has('department') ? 'is-invalid' : '' }}" type="text" name="department" id="department" value="{{ old('department', Auth::user()->department) }}" required>
                @if($errors->has('department'))
                    <div class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
            </div>				
            <div class="form-group">
                <label for="certificate_logo">{{ trans('cruds.certificate.fields.certificate_logo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('certificate_logo') ? 'is-invalid' : '' }}" id="certificate_logo-dropzone">
                </div>
                @if($errors->has('certificate_logo'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_logo') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.certificate_logo_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="stamp">{{ trans('cruds.certificate.fields.stamp') }}</label>
                <div class="needsclick dropzone {{ $errors->has('stamp') ? 'is-invalid' : '' }}" id="stamp-dropzone">
                </div>
                @if($errors->has('stamp'))
                    <div class="invalid-feedback">
                        {{ $errors->first('stamp') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.stamp_helper') }}</span>
            </div>			
            <div class="form-group">
                <label class="required" for="certificate_signature">{{ trans('cruds.certificate.fields.certificate_signature') }}</label>
                <div class="needsclick dropzone {{ $errors->has('certificate_signature') ? 'is-invalid' : '' }}" id="certificate_signature-dropzone">
                </div>
                @if($errors->has('certificate_signature'))
                    <div class="invalid-feedback">
                        {{ $errors->first('certificate_signature') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.certificate_signature_helper') }}</span>
            </div>			
			
            <div class="form-group d-none">
                <label for="empid">{{ trans('cruds.certificate.fields.empid') }}</label>
                <input class="form-control {{ $errors->has('empid') ? 'is-invalid' : '' }}" type="number" name="empid" id="empid" value="{{ old('empid', '0') }}" step="1">
                @if($errors->has('empid'))
                    <div class="invalid-feedback">
                        {{ $errors->first('empid') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.certificate.fields.empid_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/certificates/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $certificate->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

<script>
    Dropzone.options.certificateLogoDropzone = {
    url: '{{ route('admin.certificates.storeMedia') }}',
    maxFilesize: 5, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 5,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="certificate_logo"]').remove()
      $('form').append('<input type="hidden" name="certificate_logo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="certificate_logo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($certificate) && $certificate->certificate_logo)
      var file = {!! json_encode($certificate->certificate_logo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $certificate->certificate_logo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="certificate_logo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>

<script>
    Dropzone.options.certificateSignatureDropzone = {
    url: '{{ route('admin.certificates.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="certificate_signature"]').remove()
      $('form').append('<input type="hidden" name="certificate_signature" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="certificate_signature"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($certificate) && $certificate->certificate_signature)
      var file = {!! json_encode($certificate->certificate_signature) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $certificate->certificate_signature->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="certificate_signature" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
<script>
    Dropzone.options.stampDropzone = {
    url: '{{ route('admin.certificates.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="stamp"]').remove()
      $('form').append('<input type="hidden" name="stamp" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="stamp"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($certificate) && $certificate->stamp)
      var file = {!! json_encode($certificate->stamp) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $certificate->stamp->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="stamp" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection