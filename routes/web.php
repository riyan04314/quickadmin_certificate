<?php

Route::get('imagickdev', 'HomeController@imagickdev')->name('imagickdev');
/*
Route::get('qrcode', function () {
	\QrCode::size(500)->format('png')->generate('qrcode.com', public_path('images/qrcode.png'));
	dd(99);
	//return view('employees/qrCode');
});	
*/



Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
	
	
	
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/media', 'UsersController@storeMedia')->name('users.storeMedia');
    Route::post('users/ckmedia', 'UsersController@storeCKEditorImages')->name('users.storeCKEditorImages');
    Route::resource('users', 'UsersController');

    // Employees
    Route::post('employees/addcertificate/{id}', 'EmployeeController@addcertificate')->name('employees.addcertificate');
	Route::get('employees/listing', 'EmployeeController@listing')->name('employees.listing');    
	Route::delete('employees/destroy', 'EmployeeController@massDestroy')->name('employees.massDestroy');
    Route::post('employees/parse-csv-import', 'EmployeeController@parseCsvImport')->name('employees.parseCsvImport');
    Route::post('employees/process-csv-import', 'EmployeeController@processCsvImport')->name('employees.processCsvImport');
    Route::resource('employees', 'EmployeeController');
	
	Route::get('internalemployees/showall', 'EmployeeinternalController@index')->name('internalemployees.showall');
	
    Route::get('internalemployees/search', 'EmployeeinternalController@empSearch')->name('internalemployees.empSearch');
    Route::post('internalemployees/search', 'EmployeeinternalController@empSearch')->name('internalemployees.empSearch');
    Route::delete('internalemployees/destroy', 'EmployeeinternalController@massDestroy')->name('internalemployees.massDestroy');
    Route::post('internalemployees/parse-csv-import', 'EmployeeinternalController@parseCsvImport')->name('internalemployees.parseCsvImport');
    Route::post('internalemployees/process-csv-import', 'EmployeeinternalController@processCsvImport')->name('internalemployees.processCsvImport');
    Route::resource('internalemployees', 'EmployeeinternalController');
	
	/*****
	Route::post('employees/parse-csv-import', 'EmployeeController@parseCsvImport')->name('employees.parseCsvImport');
    Route::post('employees/process-csv-import', 'EmployeeController@processCsvImport')->name('employees.processCsvImport');
    	
	
	/******/
	
	
	Route::get('employees/certificateapprove/{empid}', 'EmployeeController@certificateapprove')->name('employees.certificateapprove');
	Route::get('employees/certificatereview/{empid}', 'EmployeeController@certificatereview')->name('employees.certificatereview');
	Route::get('employees/certificatepdf/{empid}', 'EmployeeController@certificatepdf')->name('employees.certificatepdf');
	

	
	
    // Certificates
    Route::delete('certificates/destroy', 'CertificateController@massDestroy')->name('certificates.massDestroy');
    Route::post('certificates/media', 'CertificateController@storeMedia')->name('certificates.storeMedia');
    Route::post('certificates/ckmedia', 'CertificateController@storeCKEditorImages')->name('certificates.storeCKEditorImages');
    Route::resource('certificates', 'CertificateController');
	
    // Departments
    Route::delete('departments/destroy', 'DepartmentController@massDestroy')->name('departments.massDestroy');
    Route::resource('departments', 'DepartmentController');	
	
    // Members
	Route::get('members/all', 'EmployeeController@allmember')->name('members.all');
    Route::delete('members/destroy', 'MemberController@massDestroy')->name('members.massDestroy');
    Route::post('members/parse-csv-import', 'MemberController@parseCsvImport')->name('members.parseCsvImport');
    Route::post('members/process-csv-import', 'MemberController@processCsvImport')->name('members.processCsvImport');
    Route::resource('members', 'MemberController');	
	
    // Internalmembers
    Route::delete('internalmembers/destroy', 'InternalmemberController@massDestroy')->name('internalmembers.massDestroy');
    Route::post('internalmembers/parse-csv-import', 'InternalmemberController@parseCsvImport')->name('internalmembers.parseCsvImport');
    Route::post('internalmembers/process-csv-import', 'InternalmemberController@processCsvImport')->name('internalmembers.processCsvImport');
    Route::resource('internalmembers', 'InternalmemberController');	
	
	Route::get('/democode','EmployeeController@democode');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
