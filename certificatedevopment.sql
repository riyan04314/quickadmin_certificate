-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2020 at 08:44 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newcertificatedevopment`
--

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `certificate_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificate_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `certificate_title`, `certificate_subtitle`, `certificate_details`, `department`, `created_by`, `empid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'شــــــــــهــادة تــكـــــريـــــم', 'تتقدم الإدارة العامة للدفاع المدني - دبي بخالص الشكر والتقدير إلى', '<p>تقديرا لجهوده الفعالة في تقديم اقتراحا مجديا يسهم في تطوير العمل بما يحقق الأهداف الاستراتيجيةمع تمنياتنا له بالتوفيق والنجاح.</p>', 'EngDept', 15, 0, '2020-06-14 21:35:43', '2020-06-16 01:52:43', NULL),
(2, 'Certificate Of Medical Introduction', 'Semester Certificate Introduction Of Business From UAE College', '<p><i><strong>Lorem ipsum</strong></i>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.</p>', 'MedDept', 16, 0, '2020-06-16 01:23:34', '2020-06-16 01:23:34', NULL),
(3, 'Certificate Of Eng Introduction', 'New Semester Certificate Introduction Of Eng From UAE College', '<p><i><strong>New Lorem ipsum</strong></i>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book</p>', 'EngDept', 15, 0, '2020-06-16 01:24:16', '2020-06-16 01:24:16', NULL),
(4, 'Certificate Of Medical Introduction', 'Semester Certificate Introduction Of Cvil From UAE College', '<p><i><strong>Lorem ipsum</strong></i>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.</p>', 'CvilManager	', 17, 0, '2020-06-16 01:23:34', '2020-06-16 01:23:34', NULL),
(5, 'Certificate of Engineer', 'Certificate of Engineer of UAE, Highest Degree..', '<p><i><strong>Lorem ipsum</strong></i>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.</p>', 'EngDept', 15, 0, '2020-06-17 11:58:19', '2020-06-17 12:04:26', NULL),
(6, 'Certificate Of Aviation Eng of UAE', 'Certificate Of Aviation Eng of UAE From UAE College', '<p><i><strong>Lorem ipsum</strong></i>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.</p>', 'EngDept', 15, 0, '2020-06-17 12:00:48', '2020-06-17 12:04:06', NULL),
(7, 'Certificate Of Introduction', 'New Semester Certificate Of Introduction From UAE College', NULL, 'EngDept', 15, 0, '2020-06-17 12:04:46', '2020-06-17 12:05:02', '2020-06-17 12:05:02');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'رئيس اللجنة للليلقعشش شسيتنا شيفغهه يثحشق', '2020-06-15 18:30:00', '2020-06-15 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `reviewed_by` int(11) NOT NULL DEFAULT 0,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT 0,
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_date` date DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `emp_id`, `emailid`, `first_name`, `last_name`, `created_by`, `reviewed_by`, `institution_name`, `department`, `default_certificate`, `certificate_approve_by`, `certificate_qrcode`, `certificate_approve_date`, `employee_type`, `certificate_approval_status`, `emp_category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, 'Eng', 'Emp011', 19, 0, 'Institute of Medical College', 'EngDept', 1, 0, NULL, NULL, '1', '1', '1', '2020-06-17 11:27:52', '2020-06-17 11:31:10', NULL),
(2, NULL, NULL, 'Eng', 'Emp 01', 19, 0, 'Institute of Eng College', 'EngDept', 3, 0, NULL, NULL, '1', '1', '1', '2020-06-17 11:40:13', '2020-06-17 11:50:36', NULL),
(3, NULL, NULL, 'ذاكر', 'حسين', 19, 18, 'Institute of NEW College', 'EngDept', 1, 15, NULL, NULL, '1', '2', '1', '2020-06-17 11:51:32', '2020-06-17 12:52:54', NULL),
(4, NULL, NULL, 'Med', 'Officer', 16, 0, 'Institute of Medical College', 'MedDept', 2, 0, NULL, NULL, '1', '1', '1', '2020-06-17 12:07:18', '2020-06-17 12:08:21', NULL),
(5, NULL, NULL, 'Med', 'Emp', 21, 0, 'Institute of Medical College', 'MedDept', 2, 0, NULL, NULL, '1', '1', '1', '2020-06-17 12:11:07', '2020-06-17 12:11:07', NULL),
(6, NULL, NULL, 'Engx', 'Bema', 20, 18, 'Institute of Eng College', 'EngDept', 5, 0, NULL, NULL, '1', '4', '1', '2020-06-17 12:37:05', '2020-06-17 12:50:32', NULL),
(7, NULL, NULL, 'Engx', 'Bemachandan', 20, 0, 'Institute of NEW College', 'EngDept', 6, 0, NULL, NULL, '1', '1', '1', '2020-06-17 12:37:25', '2020-06-17 12:37:25', NULL),
(8, 'AR1452985', NULL, 'Jhon', 'Das', 19, 0, 'Institute of Medical College', 'EngDept', 1, 0, NULL, NULL, '1', '1', '1', '2020-06-17 22:12:25', '2020-06-17 22:13:45', NULL),
(9, 'AR145244', NULL, 'Test', 'Cronie', 19, 0, 'Institute of Medical College', 'EngDept', 1, 0, NULL, NULL, '1', '1', '1', '2020-06-17 22:18:43', '2020-06-17 22:18:43', NULL),
(10, 'AR1452232', 'test01@gmail.com', 'Riyan', 'Cronie', 19, 0, 'Institute of Medical College', 'EngDept', 1, 0, NULL, NULL, '1', '1', '1', '2020-06-17 22:20:42', '2020-06-17 22:20:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Certificate', 1, 'certificate_logo', '5ee6e584d78e9_united-arab-emirates-university-squarelogo', '5ee6e584d78e9_united-arab-emirates-university-squarelogo.png', 'image/png', 'public', 51211, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 1, '2020-06-14 21:35:43', '2020-06-14 21:35:44'),
(2, 'App\\User', 2, 'signature', '5ee6ed6b3d9a4_signature', '5ee6ed6b3d9a4_signature.png', 'image/png', 'public', 1543, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 2, '2020-06-14 22:09:26', '2020-06-14 22:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2020_06_15_000001_create_media_table', 1),
(8, '2020_06_15_000002_create_permissions_table', 1),
(9, '2020_06_15_000003_create_roles_table', 1),
(10, '2020_06_15_000004_create_users_table', 1),
(11, '2020_06_15_000005_create_employees_table', 1),
(12, '2020_06_15_000006_create_certificates_table', 1),
(13, '2020_06_15_000007_create_permission_role_pivot_table', 1),
(14, '2020_06_15_000008_create_role_user_pivot_table', 1),
(15, '2020_06_16_000007_create_departments_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(17, 'employee_create', NULL, NULL, NULL),
(18, 'employee_edit', NULL, NULL, NULL),
(19, 'employee_show', NULL, NULL, NULL),
(20, 'employee_delete', NULL, NULL, NULL),
(21, 'employee_access', NULL, NULL, NULL),
(22, 'certificate_create', NULL, NULL, NULL),
(23, 'certificate_edit', NULL, NULL, NULL),
(24, 'certificate_show', NULL, NULL, NULL),
(25, 'certificate_delete', NULL, NULL, NULL),
(26, 'certificate_access', NULL, NULL, NULL),
(27, 'profile_password_edit', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(3, 1),
(3, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 20),
(3, 21),
(3, 22),
(3, 23),
(3, 24),
(3, 25),
(3, 26),
(3, 27),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(4, 24),
(4, 26),
(4, 27),
(5, 17),
(5, 18),
(5, 19),
(5, 20),
(5, 21),
(5, 24),
(5, 26),
(5, 27),
(4, 14),
(4, 16),
(4, 12),
(4, 13),
(4, 15),
(4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'User', NULL, NULL, NULL),
(3, 'Manager', '2020-06-14 22:12:30', '2020-06-14 22:12:30', NULL),
(4, 'Officer', '2020-06-16 00:31:00', '2020-06-16 00:31:00', NULL),
(5, 'Agent', '2020-06-16 01:39:37', '2020-06-16 01:39:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(15, 3),
(16, 3),
(17, 3),
(18, 4),
(19, 5),
(20, 5),
(21, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `created_by`, `name`, `signature_name`, `department`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'Admin', NULL, '', 'admin@admin.com', NULL, '$2y$10$NL0mRN8vZMV4Wh/6QKSpb.kcTWq3cW/88l4wRJaOKja7UMtepN6Uu', 'Tz5JiEfvwAjvnfYaiGLUecXPAYIIYzUOk7b9vjllRrrhNoLPkDy6kRNeG3z1', NULL, '2020-06-16 01:49:21', NULL),
(15, 1, 'Eng Manager', 'نتاايب استيباتسيب انتسيابسي', 'EngDept', 'eng_manager@gmail.com', NULL, '$2y$10$Aj6ug4.oz3kZvbYVkU7HAuEKKrA7Ao5PMz.KbYU4Is3XEPH9bdfaG', 'btuglkckrgOTECdE1Fy1cmCJbHbPBWuxtCSONJahj6UTfrCo8embUdtuU2Su', '2020-06-17 10:38:52', '2020-06-17 10:42:23', NULL),
(16, 1, 'Med Manager', 'استيباتسيب انتسيابسي', 'MedDept', 'med_manager@gmail.com', NULL, '$2y$10$98Xts18xdClRYVcXLdQ5FuL3BQxShCNRlYruLNrNA0cQh9IDCndjG', NULL, '2020-06-17 10:42:08', '2020-06-17 10:42:08', NULL),
(17, 1, 'Cvil Manager', 'استيباتسيب انتسيابسي', 'CvilManager', 'cvil_manager@gmail.com', NULL, '$2y$10$y9CcZH2.NrYy3krj1SBM9uhpIqrxSq09V857qcJTD9Z5wI.MN79Hy', 'AEAt03Yqlo77X1wfkzzIaQDwEF9wN6QPdphKTLZLsb3PN1IJPF9gMtgti7hF', '2020-06-17 10:43:47', '2020-06-17 10:43:47', NULL),
(18, 15, 'Eng Officer', NULL, 'EngDept', 'eng_officer@gmail.com', NULL, '$2y$10$q0kPA74ym2Z8914gWS41wOYxjF1dbN03jX5EU2FaeHAYFi4tag0wW', 'Lt7VvjXVFdi1GkI54sxSvsg99Mtsm7OqUNn4HCFFeNnn88fht0mn5VXMLm8w', '2020-06-17 10:48:33', '2020-06-17 10:57:10', NULL),
(19, 18, 'Eng Agent', NULL, 'EngDept', 'eng_agent@gmail.com', NULL, '$2y$10$kx4rtDKRWLGDfheFlcTK1ul0Wr.np6.OekOnfFR2r.vJD3fmKNrJu', 'SRzLaBCUCvLxgsdGXSwJVDWOK9uFbDWsZ6mIYFM1g6Fzs7Wf8IYdjuVXrlLo', '2020-06-17 10:55:53', '2020-06-17 10:55:53', NULL),
(20, 18, 'Eng Agent X', NULL, 'EngDept', 'eng_agentx@gmail.com', NULL, '$2y$10$I66Qap79Ql2TbEYIYuFHFOp9UbaLypmroLrS0VFJ7LkN3FVe0i5ze', '0b2mdjWxYMueSI1nvNWBCBXH2tmQiyFgtiaYSaFB6Jfr2u81riKNRm5gRGKy', '2020-06-17 11:01:33', '2020-06-17 11:01:33', NULL),
(21, 16, 'Med Officer', NULL, 'MedDept', 'med_officer@gmail.com', NULL, '$2y$10$lTidbwe2EqgxIejbk2LXseUpMbOecnR1Q8sBF7olJDF2K5PlvTfxm', NULL, '2020-06-17 12:09:03', '2020-06-17 12:09:03', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_1645052` (`role_id`),
  ADD KEY `permission_id_fk_1645052` (`permission_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_1645061` (`user_id`),
  ADD KEY `role_id_fk_1645061` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_id_fk_1645052` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_id_fk_1645052` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_id_fk_1645061` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_fk_1645061` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
