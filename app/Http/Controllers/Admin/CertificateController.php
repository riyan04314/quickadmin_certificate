<?php

namespace App\Http\Controllers\Admin;

use App\Certificate;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCertificateRequest;
use App\Http\Requests\StoreCertificateRequest;
use App\Http\Requests\UpdateCertificateRequest;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Auth;
//

class CertificateController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('certificate_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles[0]->title == 'Admin'){
			$certificates = Certificate::get();
		}
		else{
			$certificates = Certificate::where('department',Auth::user()->department)->get();
		}
		

        return view('admin.certificates.index', compact('certificates'));
    }

    public function create()
    {
        abort_if(Gate::denies('certificate_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.certificates.create');
    }

    public function store(StoreCertificateRequest $request)
    {
        $certificate = Certificate::create($request->all());

        if ($request->input('certificate_logo', false)) {
            $certificate->addMedia(storage_path('tmp/uploads/' . $request->input('certificate_logo')))->toMediaCollection('certificate_logo');
        }

        if ($request->input('certificate_signature', false)) {
            $certificate->addMedia(storage_path('tmp/uploads/' . $request->input('certificate_signature')))->toMediaCollection('certificate_signature');
        }		

        if ($request->input('stamp', false)) {
            $certificate->addMedia(storage_path('tmp/uploads/' . $request->input('stamp')))->toMediaCollection('stamp');
        }
		
        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $certificate->id]);
        }

        return redirect()->route('admin.certificates.index');
    }

    public function edit(Certificate $certificate)
    {
        abort_if(Gate::denies('certificate_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		//dd($certificate);
        return view('admin.certificates.edit', compact('certificate'));
    }

    public function update(UpdateCertificateRequest $request, Certificate $certificate)
    {
		$certificate->update($request->all());

        if ($request->input('certificate_logo', false)) {
            if (!$certificate->certificate_logo || $request->input('certificate_logo') !== $certificate->certificate_logo->file_name) {
                $certificate->addMedia(storage_path('tmp/uploads/' . $request->input('certificate_logo')))->toMediaCollection('certificate_logo');
            }
        } elseif ($certificate->certificate_logo) {
            $certificate->certificate_logo->delete();
        }

        if ($request->input('certificate_signature', false)) {
            if (!$certificate->certificate_signature || $request->input('certificate_signature') !== $certificate->certificate_signature->file_name) {
                $certificate->addMedia(storage_path('tmp/uploads/' . $request->input('certificate_signature')))->toMediaCollection('certificate_signature');
            }
        } elseif ($certificate->certificate_signature) {
            $certificate->certificate_signature->delete();
        }

        if ($request->input('stamp', false)) {
            if (!$certificate->stamp || $request->input('stamp') !== $certificate->stamp->file_name) {
                $certificate->addMedia(storage_path('tmp/uploads/' . $request->input('stamp')))->toMediaCollection('stamp');
            }
        } elseif ($certificate->stamp) {
            $certificate->stamp->delete();
        }
		
        return redirect()->route('admin.certificates.index');
    }

    public function show(Certificate $certificate)
    {
        abort_if(Gate::denies('certificate_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.certificates.show', compact('certificate'));
    }

    public function destroy(Certificate $certificate)
    {
        abort_if(Gate::denies('certificate_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certificate->delete();

        return back();
    }

    public function massDestroy(MassDestroyCertificateRequest $request)
    {
        Certificate::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('certificate_create') && Gate::denies('certificate_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Certificate();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
