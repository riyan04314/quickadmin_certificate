<?php

namespace App\Http\Requests;

use App\Employee;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreEmployeeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('employee_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'first_name'               => [
                'max:255',
                'required',
            ],
            'last_name'                => [
                'max:255',
            ],
            'institution_name'         => [
                'max:255',
            ],
            'default_certificate'      => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'created_by'               => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'certificate_approve_by'   => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'certificate_approve_date' => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'emp_category'             => [
                'required',
            ],
        ];
    }
}
