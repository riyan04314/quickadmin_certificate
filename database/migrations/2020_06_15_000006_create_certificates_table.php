<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('certificate_title');
            $table->string('certificate_subtitle')->nullable();
            $table->longText('certificate_details')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('empid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
